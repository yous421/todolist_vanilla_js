var toDoListModule = (function() {
    var Component;
    var ToDoList;
    
    (function() { //singleton pattern
        var instance;
        Component = function() {
            if (instance) {
                return instance;
            }  
            instance = this;
        };
    }());

    Component.prototype.searchInput = function() {
        var elements = '';
        elements += '<input id="searchInput" type="text" value="" placeholder="Search"/>';
        return elements;
    };

    Component.prototype.sortList = function() {
        var elements = '';
        elements += '<a href="javascript:void();" id="sort">Sort</a>';
        return elements;
    };

    Component.prototype.addToDo = function() {
        var elements = '';
        elements += '<button id="add">Add</button>';
        return elements;
    };

    Component.prototype.addInput = function() {
        var elements = '';
        elements += '<input id="addInput" type="text" value="" placeholder="Add"/>';
        return elements;
    };

    Component.prototype.cleanSearch = function() {
        var elements = '';
        elements += '<button id="cleanSearch" />Clean</button>';
        return elements;
    };

    Component.prototype.topArea = function() {
        var elements = '';
        elements += '<h1>To Do List</h1>';
        elements += '<p>Developed by Vanilla JS</p>';
        elements += this.searchInput();
        elements += this.cleanSearch();
        elements += '<br/>';
        elements += this.addInput();
        elements += this.addToDo();
        elements += '<br/>';
        elements += this.sortList();
        return elements;
    };

    Component.prototype.detailData = function(data) {
        var elements = '';
        data.forEach((value, index)  => {
            elements += '<li index="' + index + '">';
            elements += '<input type="checkbox">';
            elements += '<a>';
            elements += value;
            elements += '</a>';
            elements += '<button id="delete">Delete</button>';
            elements += '</li>';
        });
        return elements;
    };

    Component.prototype.toDoList = function(data) {
        var elements = '';
        elements += '<ul id="toDoList">';
        elements += this.detailData(data);
        elements += '</ul>'; 
        return elements;
    };
    
    (function() { //singleton pattern
        var instance
        ToDoList = function() {
            if (instance) {
                instance = this;
            }
            
            this.data = toDoListData;
            this.sortType = true; // true: ascending, false: descending
            return instance;
        };
    }());

    ToDoList.prototype.display = function() {
        var app = document.getElementById('app');
        app.innerHTML = '';
        this.todolist = new Component();
    
        app.innerHTML += this.todolist.topArea();
        app.innerHTML += this.todolist.toDoList(this.data);
    };

    ToDoList.prototype.add = function() {
        var addInput = document.getElementById('addInput');
        this.data.push(addInput.value);
        this.display();
    };

    ToDoList.prototype.delete = function(e) {
        var ul = e.target.parentNode.parentNode;
        var li = e.target.parentNode;
        var index = li.getAttribute('index');

        this.data.splice(index, 1);
        ul.removeChild(li);
        this.display();
    };

    ToDoList.prototype.sort = function() {
        this.data.sort();
        this.data.sort((a, b) => {
            return this.sortType ? a - b : b - a;
        });
        
        this.sortType = !this.sortType;
        this.display();
    };

    ToDoList.prototype.search = function() {
        var keyWord = document.getElementById('searchInput').value;
        
        if (keyWord.length > 0) {
            var searchResult = this.data.filter(todo => {
                return todo.toString().indexOf(keyWord) > -1
            });

            var app = document.getElementById('app');
            var toDoList = document.getElementById('toDoList');
            
            if (toDoList) {
                app.removeChild(toDoList);
            }

            if (searchResult.length > 0) {
                app.innerHTML += this.todolist.toDoList(searchResult);
            }
        } else {
            this.display();
        }
    };

    ToDoList.prototype.edit = function(e) {
        var current = e.target;
        var parent = current.parentNode;
        var sibling = current.nextSibling;
        var text = current.textContent;

        var newElement = document.createElement('input');
        newElement.id = 'editInput';
        newElement.type = 'text';
        newElement.value = text;

        parent.removeChild(current);
        parent.insertBefore(newElement, sibling)
    };

    ToDoList.prototype.update = function(e) {
        var current = e.target;
        var parent = current.parentNode;
        var sibling = current.nextSibling;
        var index = parent.getAttribute('index');

        this.data[index] = current.value;

        var newElement = document.createElement('a');
        newElement.textContent = current.value;

        parent.removeChild(current);
        parent.insertBefore(newElement, sibling);
    };

    ToDoList.prototype.check = function(e) {
        var sibling = e.target.nextSibling;
        var styles = ''
        if (!sibling.getAttribute('style')) {
            styles = 'text-decoration: line-through';
        }
        sibling.setAttribute('style', styles);
    };

    ToDoList.prototype.eventListenAll = function() {
        var current = this;
        utils.addEvent(window, 'click', function(e){
            if (e.target.closest('#add')) current.add();
            if (e.target.closest('#sort')) current.sort();
            if (e.target.closest('#delete')) current.delete(e);
            if (e.target.closest('#toDoList li a')) current.edit(e);
            if (e.target.closest('#toDoList li input[type=checkbox]')) current.check(e);
            if (e.target.closest('#cleanSearch')) current.display(e);
        });

        utils.addEvent(window, 'change', function(e){
            if (e.target.closest('#searchInput')) current.search();
        });

        utils.addEvent(window, 'mouseout', function(e){
            if (e.target.closest('#editInput')) current.update(e);
        });
    };

    return {
        ToDoList: ToDoList
    };
}());